#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>
#include <time.h>
#include <omp.h>

// OMP_NUM_THREADS=3 ./omp 10 3
// unset OMP_NUM_THREADS && ./omp 10 3

void fun(double *x, int nx, double *ans, int *th) {
  #pragma omp parallel for
  for (int i=0; i<nx; i++) {
    ans[i] = x[i] * x[i];
    th[i] = omp_get_thread_num();
  }
}

int main(int argc, char **argv) {
  int verbose=0;
  if (argc != 3) {
    printf("Usage: ./omp ncols nrows\n");
    exit(1);
  }
  clock_t total_begin = clock();
  clock_t begin, end;
  double time_spent;
  if (verbose) printf("parsing command line args\n");
  int nc = atoi(argv[1]);
  int nr = atoi(argv[2]);
  if (verbose) printf("allocation of double array for source\n");
  begin = clock();
  double **x = malloc(nc * sizeof *x);
  for (int i=0; i<nc; i++) x[i] = malloc(nr * sizeof *x[i]);
  // populate source columns
  if (verbose) printf("populating source data\n");
  for (int j=0; j<nc; j++) {
    for (int i=0; i<nr; i++) {
      x[j][i] = sqrt((double)rand());
    }
  }
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("allocating and populating source data took: %.2fs\n", time_spent);
  
  if (verbose) printf("allocation of double array for answer\n");
  begin = clock();
  double **ans = malloc(nc * sizeof *ans);
  for (int i=0; i<nc; i++) ans[i] = malloc(nr * sizeof *ans[i]);
  
  double* dans[nc];                                             // pointers to answer columns
  double* dx[nc];                                               // pointers to source columns
  for (int j=0; j<nc; j++) {
    dans[j] = ans[j];
    dx[j] = x[j];
  }
  // which thread did computation
  //int th[nc][nr]; // segfault, thus malloc below
  if (verbose) printf("running malloc for th**\n");
  int **th = malloc(nc * sizeof *th);
  for (int i=0; i<nc; i++) th[i] = malloc(nr * sizeof *th[i]);
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("allocating memory for answers took: %.2fs\n", time_spent);
  
  int maxth = omp_get_max_threads();
  // enable nested parallelism
  omp_set_nested(1);
  if (verbose) printf("running nested parallel function\n");
  begin = clock();
  #pragma omp parallel for
  for (int j=0; j<nc; j++) {
    fun(x[j], nr, ans[j], th[j]);
  }
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("main parallel loop took: %.2fs\n", time_spent);
  // check what iteration was computed by which thread - only for small input
  /*printf("# iteration [col][row]\n");
  for (int j=0; j<nc; j++) {
    for (int i=0; i<nr; i++) {
      printf("[%02d][%02d] made by thread %02d\n", j+1, i+1, th[j][i]+1);
    }
  }*/
  
  // iterations by thread
  if (verbose) printf("zero-ing iterth array\n");
  begin = clock();
  int iterth[maxth];
  for (int i=0; i<maxth; i++) {
    iterth[i] = 0;
  }
  printf("# post processing summary\n");
  for (int j=0; j<nc; j++) {
    for (int i=0; i<nr; i++) {
      iterth[th[j][i]]++;
    }
  }
  if (verbose) printf("printing iterations by thread\n");
  setlocale(LC_NUMERIC, "");
  for (int i=0; i<maxth; i++) {
    printf("## thread %02d made %'d iterations\n", i+1, iterth[i]);
  }
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("summarizing processing took: %.2fs\n", time_spent);
  
  clock_t total_end = clock();
  double total_time_spent = (double)(total_end - total_begin) / CLOCKS_PER_SEC;
  printf("# total threads: %d\n# total iterations: %'d\n# total time: %.2fs\n", maxth, nc*nr, total_time_spent);
  return 0;
}
