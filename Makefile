
.PHONY: clean
clean:
	rm ./omp

.PHONY: build
build:
	gcc -fopenmp -O3 omp.c -lm -o omp
